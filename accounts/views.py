from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .forms import CreateUserForm
# Create your views here.

def registerPage(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            messages.success(request, "Your account created successfully, please login")
            return redirect('/')
    context = {'form':form}
    return render(request, 'registration/signup.html', context)

def loginPage(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('homepage/homepage.html')
        else:
            messages.info(request, 'Username or password is incorrect')
    context = {}
    return render(request, 'registration/login.html', context)

def logoutPage(request):
    logout(request)
    messages.success(request, "Logout successfully")
    return redirect('accounts:loginPage')
