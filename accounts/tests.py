from django.test import TestCase, Client

# Create your tests here.
class Testing(TestCase):
    def test_url_login(self):
        response = Client().get('/accounts/login/')
        self.assertEquals(response.status_code, 200)
