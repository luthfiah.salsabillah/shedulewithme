from django.db import models

# Create your models here.
from django.db.models import CharField

# Create your models here.
class WebinarAdd(models.Model):
    nama = models.CharField(max_length=50)
    tema =  models.CharField(max_length=50)
    pemateri = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=200)
    institusi = models.CharField(max_length=50)
    linkPendaftaran = models.CharField(max_length=50)

    def __str__(self):
        return self.nama