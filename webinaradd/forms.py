from django import forms

from .models import WebinarAdd

class FormWebinarAdd(forms.ModelForm):
    nama = forms.CharField()
    tema =  forms.CharField()
    pemateri = forms.CharField()
    deskripsi = forms.CharField()
    institusi = forms.CharField()
    linkPendaftaran = forms.CharField()

    class Meta:
        model = WebinarAdd
        fields = '__all__'