from django.shortcuts import render,get_object_or_404, redirect
from django.http import HttpResponse, Http404
from .models import WebinarAdd
from .forms import FormWebinarAdd

# Create your views here.

def index(request):
    webinarobject = WebinarAdd.objects.all()
    return render(request, 'webinaradd/webinar_list.html', {'webinarobject':webinarobject})

def formWebinar(request):
    form = FormWebinarAdd()
    if request.method == 'POST':
        form = FormWebinarAdd(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/ListWebinar')
    return render(request, 'webinaradd/webinar_form.html', {'form':form})

def webinarDetail(request,id):
    jadwalwebinar = WebinarAdd.objects.get(id=id)
    return render(request,'webinaradd/webinar_details.html',{'jadwalwebinar':jadwalwebinar})

def deleteWebinar(request,id):
    item = WebinarAdd.objects.get(id=id)
    if request.method == 'POST':
        item.delete()
        return redirect('/ListWebinar')
    return render(request, 'webinaradd/webinar_delete.html',{'item':item})
