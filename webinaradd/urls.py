from django.urls import path
from . import views

app_name = 'webinaradd'

urlpatterns = [
    path('ListWebinar', views.index, name='webinar_list'),
    path('FormWebinar',views.formWebinar, name='webinar_form'),
    path('<int:id>',views.webinarDetail),
    path('deleteWebinar/<str:id>', views.deleteWebinar,name="webinar_delete")
]